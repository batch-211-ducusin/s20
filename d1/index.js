console.log("Hello");

//Repetition Control Structures (loops)
	/*
		- loops are one of the most important feature that programming must have.
		- it lets us executee code repeatedly in a pre-set number or maybe forever.
	*/
	/*
		Mini Activity
		1. Create a function named greeting() and display "Hi, Batch 211!" using console.log inside the function.
		2. Invoke the greeting() function 10 times
		3. take a screenshot of your work's console and send it to the bacth hangouts.
	*/
/*	function greeting(){
		console.log("Hi, Batch 211!");
	}
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();*/

	//we can just use loops

/*	let countNum = 10;
	while(countNum !== 0){
		console.log("This is printed inside while loop: "+countNum);
		greeting()
		countNum--;
	}*/


//While Loops
	/*
		- A while loop takes in an expression or condition.
		if the condition evaluates to true, the statement inside the code block will be executed.

		Syntax:
			while(condition){
				<code block>
				<final expression ++ or -->
			}
		Condition
			- This are the unit of code that is being evaluated in our loop. loop will run while the condition or expression is tru
		Code Block
			- Codes that will be executed several times.
		Final Expression
			- Idicates how to advance the loop.
	*/

/*	let count = 5;
	//while the value of count is not yet 0. mag loloop sya.
	while(count !== 0){
		//the current value of count is printed out.
		console.log("While: "+count);
		//decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
		count--;
	}*/

/*	let num = 20;
	while(num !== 0){
		console.log("While: num "+num);
		num--;
	}
*/
/*	let digit = 5;
	while(digit !== 20){
		console.log("While: digit "+digit);
		digit++;
	}*/

/*	let num1 = 1;
	while(num1 === 2){
		console.log("While: num1 " + num1);
		num--;
	}	//dito hindi gagana si loop dahil umpisa palang, hindi na nameet ang condition.*/



//Do While Loop
	/*
		- A do while loop mworks a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at lease once.

		Syntax:
			do {
				statement / code block
				final expression ++ / --
			} while (expression or condition)
	*/

/*	let number = Number(prompt("Give me a number"));
	do {
		console.log("Do while: " + number);
		number += 1;
	} while(number < 10);*/

	/*
		How the Do While Loop works:
		1. The statement in the "do" block execute once
		2. The message "Do While: " + number will be printed out in the console.
		3. After execute once, the while statement will evaluate whether to run the next iteration of the loop based on the given expression or condition.
		4. If the expression or condition is not true, another iteration of the loop will be executed and will be repeated until our condition is met.
		5. If the condition is met or true, the loop will stop.
	*/




//For Loop
	/*
		- A For Loop is more flexible than while and do-while loops.
		- It consists of three parts.
			1. Initialization value that will tract the progression of the loop.
			2. Condition that will be evaluated which will determine if the loop will run one more time.
			3. Final Expression indicates how to advance the loop.

		Syntax:
		for(initialization; condition; final expression){
			statement
		}
	*/

/*	for(let count = 0; count <= 20; count++){
		console.log(`For loop: ${count}`);
	}*/

	/*
		Mini Activity
		- Refactor the code above that the loop will only print the even numbers.
		- Take a screenshot of your work and send it to our batch chat
	*/
	for(let count = 0; count <= 20; count++){
		if(count % 2 === 0 && count != 0)
		console.log(`For loop: ${count}`);
	}






//Accession Elements of a String
	/*
		- Individual characters of a string may be accessed using its index number
		- The first character in a sctring corresponds to the number 0, the next is 1...
	*/

/*	let myString = "Camille Doroteo";
	//characters in strings may be counted using the  .length property.
	//strings are special compared to other data types.
	console.log(myString.length);

	console.log(myString[2]);
	console.log(myString[0]);
	console.log(myString[8]);
	console.log(myString[14]);

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x]);
	};*/


	/*
		Mini Activity
		- create a variable named myFullName that will hold your name.
		- create a for loop.
	*/
	let myFullName = "Abdul-Johari Ducusin"
	for(let x = 0; x < myFullName.length; x++){
		console.log(myFullName[x]);
	};

	let myName = "Nehemiah";
	let myNewName = "";
	for(let i = 0; i < myName.length; i++){
		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "u" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "o"
			){
			//papalitan ang "a","i","u","e","o" ng 3 na cinonsole.log namin.
			console.log(3);
		} else {
			console.log(myName[i]);
			myNewName += myName[i];
		}
	}
	//we concatenate the characters based on the given condition.
	console.log(myNewName);




//Continue and Brake Statements
	/*
		The Continue statement allows the code to do to the next iteration of the loop without finishing the execution of all statements in a code block.

		the Break statement is used to terminate the current loop once a match has been found.
	*/

	for(let count = 0; count <= 20; count++){
		if(count % 2 === 0){
			console.log("Even Number");
			//this tell the console to continue to the next iteration of the loop.
			continue;
			//this ignores all statements located after the continue statement
			//console.log("Hello"); //hindi na sya mapiprint.
		}
		console.log("Continue and Break: "+count);

		if(count > 10){
			//number values after 10 will no longer be printed.
			//break tells the code to terminate or stop the loop even if the expression or condition of the loop defines that it should execute so long as the value of count is less than or equal to 20.
			break;
		}
	}

	let name = "alexandro";
	for(let i = 0; i < name.length; i++){
		//the current letter is printed out based on its index.
		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){
			console.log("Continue to the next iteration");
			continue;
		}

		if(name[i].toLowerCase() == "d"){
			break;
		}
	}